# Generated by Django 5.0.1 on 2024-02-02 10:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adminapp', '0005_alter_faculty_department'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='department',
            field=models.CharField(choices=[('CSE(R)', 'CSE(Regular)'), ('CSE(H)', 'CSE(Honors)'), ('CSIT', 'CS&IT')], max_length=50),
        ),
    ]
